<%--
  Created by IntelliJ IDEA.
  User: 17766
  Date: 2019/4/18
  Time: 10:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>欢迎</title>

    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim 和 Respond.js 是为了让 IE8 支持 HTML5 元素和媒体查询（media queries）功能 -->
    <!-- 警告：通过 file:// 协议（就是直接将 html 页面拖拽到浏览器中）访问页面时 Respond.js 不起作用 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-inverse visible-sm visible-md visible-lg" role="navigation">
    <ul class="nav navbar-nav navbar-left">
        <li><a href="/admin/toHome" >企业级门户应用网站</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a href="/logout"><span class="glyphicon glyphicon-log-in"></span> 登出</a></li>
    </ul>
    </div>
</nav>
<nav class="navbar navbar-inverse visible-xs" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#example-navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin/toHome">Portal</a>
        </div>
        <div class="collapse navbar-collapse" id="example-navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="/logout"><span class="glyphicon glyphicon-log-in"></span> 登出</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="row">
    <div class="col-sm-8 col-sm-offset-2 text">
        ${msg}
    </div>
    <div class="col-lg-4 col-lg-offset-4 visible-lg visible-md">
        <form name="frmlogin" method="post" action="${pageContext.request.contextPath}/login">
            <div class="form-group">
                <label for="loginInputName">用户ID</label>
                <input type="text" class="form-control" id="loginInputName" name="userId" placeholder="用户ID">
            </div>
            <div class="form-group">
                <label for="loginInputPsw">密码</label>
                <input type="password" class="form-control" id="loginInputPsw" name="password" placeholder="密码">
            </div>
            <button type="submit" class="button btn-primary btn-lg">
                <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;登录
            </button>

            <button type="button" class="btn-danger btn-lg" onclick="cancel()">
                <span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;取消
            </button>
        </form>
    </div>
    <div class="col-lg-4 col-lg-offset-4 visible-xs visible-sm">
        <form name="frmlogin" method="post" action="${pageContext.request.contextPath}/login">
            <div class="form-group">
                <label for="loginInputName2">用户ID</label>
                <input type="text" class="form-control" id="loginInputName2" name="userId" placeholder="用户ID">
            </div>
            <div class="form-group">
                <label for="loginInputPsw2">密码</label>
                <input type="password" class="form-control" id="loginInputPsw2" name="password" placeholder="密码">
            </div>
            <button type="submit" class="button btn-primary btn-lg">
                <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;登录
            </button>

            <button type="button" class="btn-danger btn-lg" onclick="cancel()">
                <span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;取消
            </button>
        </form>
    </div>
</div>

<nav class="navbar navbar-default navbar-fixed-bottom">
    <div class="container">
        <h6 align="center">@版权所有se160105组</h6>
    </div>
</nav>

<script language="JavaScript">
    function cancel() {
        document.location.replace("/");
    }
</script>
<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
</body>
</html>
