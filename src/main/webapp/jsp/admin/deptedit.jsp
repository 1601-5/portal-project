<%--
  Created by IntelliJ IDEA.
  User: 17766
  Date: 2019/4/16
  Time: 20:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>更改组织信息</title>
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim 和 Respond.js 是为了让 IE8 支持 HTML5 元素和媒体查询（media queries）功能 -->
    <!-- 警告：通过 file:// 协议（就是直接将 html 页面拖拽到浏览器中）访问页面时 Respond.js 不起作用 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<%--上方导航栏--%>
<nav class="navbar navbar-inverse visible-sm visible-md visible-lg" role="navigation">
    <ul class="nav navbar-nav navbar-left">
        <li><a href="/admin/toHome" >企业级门户应用网站</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a href="/logout"><span class="glyphicon glyphicon-log-in"></span> 登出</a></li>
    </ul>
    </div>
</nav>
<nav class="navbar navbar-inverse visible-xs" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#example-navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin/toHome">Portal</a>
        </div>
        <div class="collapse navbar-collapse" id="example-navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="/logout"><span class="glyphicon glyphicon-log-in"></span> 登出</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="row">
    <%--左侧导航条--%>
        <div class="col-xs-2 ">
            <ul class="nav nav-pills nav-stacked">
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        组织人员管理
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/admin/toDeptList">组织信息</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/admin/toPersonList">人员信息</a></li>
                    </ul>
                </li>
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        应用管理
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/admin/toAppList">应用信息</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/admin/toModuleList">模块信息</a></li>
                    </ul>
                </li>
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        授权管理
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/admin/toRoleList">角色信息</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/admin/toRoleMsg">授权管理</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    <div class="col-xs-10">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <a class="navbar-brand">修改应用</a>
            </div>
        </nav>
        <form name="frmdept" method="post" action="${pageContext.request.contextPath}/admin/deptEdit">
            <input type="hidden" name="deptId" value="${dept.deptId}">
            <div class="form-group">
                <label for="deptInputName">组织名称</label>
                <input type="text" class="form-control" id="deptInputName" name="deptName" value="${dept.deptName}">
            </div>
            <div class="form-group">
                <label for="deptInputFather">上级组织</label>
                <select class="form-control" id="deptInputFather" name="deptFather">
                    <option value=""></option>
                    <c:forEach var="father" items="${fatherlist}">
                        <option value="${father.deptId}" <c:if test="${father.deptId==dept.deptFather}">selected</c:if>>${father.deptName}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label for="deptInputNum">组织代码</label>
                <input type="number" class="form-control" id="deptInputNum" name="deptNum" value="${dept.deptNum}">
            </div>
            <div class="form-group">
                <label for="deptInputSite">办公地址</label>
                <input type="text" class="form-control" id="deptInputSite" name="deptSite" value="${dept.deptSite}">
            </div>
            <div class="form-group">
                <label for="deptInputCreate">创建时间</label>
                <input type="date" class="form-control" id="deptInputCreate" name="deptCreate" value="${dept.deptCreate}">
            </div>
            <button type="submit" class="button btn-primary btn-lg">
                <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;提交
            </button>
            <button type="reset" class="btn-info btn-lg">
                <span class="glyphicon glyphicon-refresh "></span>&nbsp;&nbsp;重置
            </button>
            <button type="button" class="btn-danger btn-lg" onclick="cancel()">
                <span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;取消
            </button>
        </form>
    </div>
</div>

<script language="JavaScript">
    function cancel() {
        document.location.replace("/admin/toDeptList");
    }
</script>
<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
</body>
</html>
