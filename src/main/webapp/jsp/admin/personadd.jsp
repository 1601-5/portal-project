<%--
  Created by IntelliJ IDEA.
  User: 17766
  Date: 2019/4/16
  Time: 22:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>注册人员</title>
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim 和 Respond.js 是为了让 IE8 支持 HTML5 元素和媒体查询（media queries）功能 -->
    <!-- 警告：通过 file:// 协议（就是直接将 html 页面拖拽到浏览器中）访问页面时 Respond.js 不起作用 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<%--上方导航栏--%>
<nav class="navbar navbar-inverse visible-sm visible-md visible-lg" role="navigation">
    <ul class="nav navbar-nav navbar-left">
        <li><a href="/admin/toHome" >企业级门户应用网站</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a href="/logout"><span class="glyphicon glyphicon-log-in"></span> 登出</a></li>
    </ul>
    </div>
</nav>
<nav class="navbar navbar-inverse visible-xs" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#example-navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin/toHome">Portal</a>
        </div>
        <div class="collapse navbar-collapse" id="example-navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="/logout"><span class="glyphicon glyphicon-log-in"></span> 登出</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="row">
    <%--左侧导航条--%>
        <div class="col-xs-2 ">
            <ul class="nav nav-pills nav-stacked">
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        组织人员管理
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/admin/toDeptList">组织信息</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/admin/toPersonList">人员信息</a></li>
                    </ul>
                </li>
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        应用管理
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/admin/toAppList">应用信息</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/admin/toModuleList">模块信息</a></li>
                    </ul>
                </li>
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        授权管理
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/admin/toRoleList">角色信息</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/admin/toRoleMsg">授权管理</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    <div class="col-xs-10">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <a class="navbar-brand">注册人员</a>
            </div>
        </nav>
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 text">
                ${msg}
            </div>
        </div>
        <form name="frmperson" method="post" action="${pageContext.request.contextPath}/admin/personAdd">
            <div class="form-group">
                <label for="moduleInputApp">所属组织</label>
                <select class="form-control" id="moduleInputApp" name="deptId">
                    <option value=""></option>
                    <c:forEach var="dept" items="${deptlist}">
                        <option value="${dept.deptId}">${dept.deptName}</option>
                    </c:forEach>
                </select>
            </div>
            <div class="form-group">
                <label for="personInputLoginId">登录ID</label>
                <input type="text" class="form-control" id="personInputLoginId" name="userId" placeholder="登录ID">
            </div>
            <div class="form-group">
                <label for="personInputIsAdmin">是否为管理员</label>
                <select class="form-control" id="personInputIsAdmin" name="isAdmin">
                    <option value="0">否</option>
                    <option value="1">是</option>
                </select>
            </div>
            <div class="form-group">
                <label for="personInputName">人员名称</label>
                <input type="text" class="form-control" id="personInputName" name="personName" placeholder="人员名称">
            </div>
            <div class="form-group">
                <label for="personInputTel">联系方式</label>
                <input type="tel" class="form-control" id="personInputTel" name="personTel" placeholder="联系方式">
            </div>
            <div class="form-group">
                <label for="personInputPwd">登录密码</label>
                <%--需要加密--%>
                <input type="password" class="form-control" id="personInputPwd" name="password" placeholder="登录密码">
            </div>
            <div class="form-group">
                <label for="personInputSex">性别</label>
                <select class="form-control" id="personInputSex" name="personSex">
                    <option value="F">女</option>
                    <option value="M">男</option>
                    <option value="C">保密</option>
                </select>
            </div>
            <div class="form-group">
                <label for="moduleInputEntertime">入职时间</label>
                <input type="date" class="form-control" id="moduleInputEntertime" name="personEntertime" placeholder="入职时间">
            </div>
            <div class="form-group">
                <label for="moduleInputPay">薪水</label>
                <input type="number" class="form-control" id="moduleInputPay" name="personPay" placeholder="薪水">
            </div>
            <button type="submit" class="button btn-primary btn-lg">
                <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;提交
            </button>
            <button type="reset" class="btn-info btn-lg">
                <span class="glyphicon glyphicon-refresh "></span>&nbsp;&nbsp;重置
            </button>
            <button type="button" class="btn-danger btn-lg" onclick="cancel()">
                <span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;取消
            </button>
        </form>
    </div>
</div>
<script language="JavaScript">
    function cancel() {
        document.location.replace("/admin/toPersonList");
    }
</script>
<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
</body>
</html>
