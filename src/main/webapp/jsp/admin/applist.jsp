<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 17766
  Date: 2019/4/12
  Time: 14:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>应用信息</title>
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim 和 Respond.js 是为了让 IE8 支持 HTML5 元素和媒体查询（media queries）功能 -->
    <!-- 警告：通过 file:// 协议（就是直接将 html 页面拖拽到浏览器中）访问页面时 Respond.js 不起作用 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-inverse visible-sm visible-md visible-lg" role="navigation">
    <ul class="nav navbar-nav navbar-left">
        <li><a href="/admin/toHome" >企业级门户应用网站</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a href="/logout"><span class="glyphicon glyphicon-log-in"></span> 登出</a></li>
    </ul>
    </div>
</nav>
<nav class="navbar navbar-inverse visible-xs" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#example-navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin/toHome">Portal</a>
        </div>
        <div class="collapse navbar-collapse" id="example-navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="/logout"><span class="glyphicon glyphicon-log-in"></span> 登出</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="row">
    <%--左侧导航条--%>
        <div class="col-xs-2 ">
            <ul class="nav nav-pills nav-stacked">
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        组织人员管理
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/admin/toDeptList">组织信息</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/admin/toPersonList">人员信息</a></li>
                    </ul>
                </li>
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        应用管理
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/admin/toAppList">应用信息</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/admin/toModuleList">模块信息</a></li>
                    </ul>
                </li>
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        授权管理
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/admin/toRoleList">角色信息</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/admin/toRoleMsg">授权管理</a></li>
                    </ul>
                </li>
            </ul>
        </div>

    <div class="col-xs-10">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <a class="navbar-brand">应用信息</a>
                <ul class="nav navbar-nav navbar-right">
                    <li>
                        <button type="button" class="btn-primary btn-lg" onclick="appadd()">
                            <span class="glyphicon glyphicon-plus"></span>&nbsp;&nbsp;注册
                        </button>
                    </li>
                    <li>
                        <button type="button" class="btn-info btn-lg" onclick="appedit()">
                            <span class="glyphicon glyphicon-pencil"></span>&nbsp;&nbsp;修改
                        </button>
                    </li>
                    <li>
                        <button type="button" class="btn-danger btn-lg" onclick="appdel()">
                            <span class="glyphicon glyphicon-remove"></span>&nbsp;&nbsp;注销
                        </button>
                    </li>
                </ul>
            </div>
        </nav>
        <form name="frmapp" method="post" action="">
            <table class="table table-hover">
                <tr>
                    <td align="center">选择</td>
                    <td align="center">应用名称</td>
                    <td align="center">应用代号</td>
                    <td align="center">应用url</td>
                </tr>
                <c:forEach var="app" items="${applist}">
                <tr>
                    <td align= "center" >
                        <input type= "radio" name= "appId" value= "${app.appId}">
                    </td >
                    <td align="center">${app.appName}</td>
                    <td align="center">${app.appNum}</td>
                    <td align="center">${app.appUrl}</td>
                    </c:forEach>
            </table>
        </form>
    </div>
</div>
<nav class="navbar navbar-default navbar-fixed-bottom">
    <div class="container">
        <h6 align="center">@版权所有se160105组</h6>
    </div>
</nav>
<script language="JavaScript">
    function appdel() {
        var appids=document.getElementsByName("appId");
        var isSelected=false;
        for(i=0;i<appids.length;i++){
            if(appids[i].checked){
                isSelected=true;
            }
        }
        if (isSelected) {
            if (confirm("确定要删除应用吗")) {
                document.frmapp.action = "${pageContext.request.contextPath}/admin/appDel";
                document.frmapp.submit();
            }
        } else {
            alert("请先选择应用");
        }
    }

    function appadd() {
        document.location.replace("${pageContext.request.contextPath}/admin/toAppAdd");
    }

    function appedit() {
        var appids=document.getElementsByName("appId");
        var isSelected=false;
        for(i=0;i<appids.length;i++){
            if(appids[i].checked){
                isSelected=true;
            }
        }
        if (isSelected) {
            document.frmapp.action = "${pageContext.request.contextPath}/admin/toAppEdit";
            document.frmapp.submit();
        } else {
            alert("请先选择应用");
        }
    }
</script>
<!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
</body>
</html>
