<%--
  Created by IntelliJ IDEA.
  User: 17766
  Date: 2019/4/17
  Time: 0:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>授权管理</title>
    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim 和 Respond.js 是为了让 IE8 支持 HTML5 元素和媒体查询（media queries）功能 -->
    <!-- 警告：通过 file:// 协议（就是直接将 html 页面拖拽到浏览器中）访问页面时 Respond.js 不起作用 -->
    <!--[if lt IE 9]>
    <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<%--上方导航栏--%>
<nav class="navbar navbar-inverse visible-sm visible-md visible-lg" role="navigation">
    <ul class="nav navbar-nav navbar-left">
        <li><a href="/admin/toHome" >企业级门户应用网站</a></li>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <li><a href="/logout"><span class="glyphicon glyphicon-log-in"></span> 登出</a></li>
    </ul>
    </div>
</nav>
<nav class="navbar navbar-inverse visible-xs" role="navigation">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#example-navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/admin/toHome">Portal</a>
        </div>
        <div class="collapse navbar-collapse" id="example-navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="/logout"><span class="glyphicon glyphicon-log-in"></span> 登出</a></li>
            </ul>
        </div>
    </div>
</nav>
<div class="row">
    <%--左侧导航条--%>
        <div class="col-xs-2 ">
            <ul class="nav nav-pills nav-stacked">
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        组织人员管理
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/admin/toDeptList">组织信息</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/admin/toPersonList">人员信息</a></li>
                    </ul>
                </li>
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        应用管理
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/admin/toAppList">应用信息</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/admin/toModuleList">模块信息</a></li>
                    </ul>
                </li>
                <li role="presentation" class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true"
                       aria-expanded="false">
                        授权管理
                    </a>
                    <ul class="dropdown-menu">
                        <li><a href="/admin/toRoleList">角色信息</a></li>
                        <li role="separator" class="divider"></li>
                        <li><a href="/admin/toRoleMsg">授权管理</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    <div class="col-xs-10">
        <nav class="navbar navbar-default" role="navigation">
            <div class="container-fluid">
                <a class="navbar-brand">授权管理</a>
                <form name="query" method="post" action="">
                    <input type="number" class="form-control" placeholder="登录ID" name="userId">
                    <button type="button" class="btn btn-success btn-lg" onclick="personquery()">查找</button>
                </form>
            </div>
        </nav>
        <div class="row">
            <div class="col-sm-8 col-sm-offset-2 text">
                ${msg}
            </div>
        </div>
        <form name="frmmsg" method="post" action="${pageContext.request.contextPath}/admin/RoleMsg" class="<c:if test="${!isExist}">invisible</c:if>">
            <div class="form-group">
                <label for="msgName">人员名称</label>
                <input type="text" class="form-control" id="msgName" value="${person.personName}">
            </div>
            <input type="hidden" name="personId" value="${person.personId}">
            <div>
                <label for="inlineCheckbox">赋予角色</label>
            </div>
            <div class="form-group">
                    <c:forEach var="role" items="${rolelist}">
                        <label class="checkbox-inline">
                            <input type="checkbox" id="inlineCheckbox" name="roleId" value="${role.roleId}"
                            <c:forEach var="personrole" items="${persomrolelist}">
                                <c:if test="${role.roleId==personrole.roleId}">checked</c:if>
                            </c:forEach>>${role.roleName}
                        </label>
                    </c:forEach>
            </div>
            <button type="submit" class="button btn-primary btn-lg">
                <span class="glyphicon glyphicon-ok"></span>&nbsp;&nbsp;提交
            </button>
            <button type="reset" class="btn-info btn-lg">
                <span class="glyphicon glyphicon-refresh "></span>&nbsp;&nbsp;重置
            </button>
        </form>
    </div>
</div>
<nav class="navbar navbar-default navbar-fixed-bottom">
    <div class="container">
        <h6 align="center">@版权所有se160105组</h6>
    </div>
</nav>
<script language="JavaScript">
    function personquery() {
        var userId = document.getElementsByName("userId");
        document.query.action = "${pageContext.request.contextPath}/admin/toRoleQuery";
        document.query.submit();
    }
</script>
<script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
<!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
</body>
</html>
