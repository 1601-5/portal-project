package com.hbxy.se160105.portal.admin.right.model;

import java.io.Serializable;

/**
 * roleright
 * @author 
 */
public class Roleright implements Serializable {
    private Integer rmId;

    private Integer roleId;

    private Integer moduleId;

    private static final long serialVersionUID = 1L;

    public Integer getRmId() {
        return rmId;
    }

    public void setRmId(Integer rmId) {
        this.rmId = rmId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }
}