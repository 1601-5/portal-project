package com.hbxy.se160105.portal.admin.util;

import javax.servlet.http.HttpServletRequest;

public class RightManager {
    //不需要权限验证的url
    private static final String[] IGNORE_URI = { "/index","/noright","/logout","/toHome" };
    public boolean hasRightByUrl(HttpServletRequest request){
        boolean hasright = false;

        String currentUrl = request.getServletPath();
        if("".equals(currentUrl)){//如果是/*的情况
            currentUrl=request.getRequestURI();//getRequestURI包含上下文根
            currentUrl=currentUrl.substring(request.getContextPath().length());//去掉上下文根
        }
//        String queryUrl = request.getQueryString();//可以注释

        String headUrl = currentUrl;

        for (String s : IGNORE_URI) {
            if (headUrl.contains(s)) {
                hasright = true;
                break;
            }
        }
        if (!hasright) {
            //从seesion 权限控制列表中判断是否包含当前访问的url
            if (UserLoginUtil.getUserLogin(request).getRightSet().contains(headUrl)) {//只需要这个就可以了
                hasright = true;
            }
        }

        return hasright;
    }
}
