package com.hbxy.se160105.portal.admin.right.service;

import com.hbxy.se160105.portal.admin.right.model.Roleright;
import com.hbxy.se160105.portal.admin.right.model.RolerightExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RolerightService {
    long countByExample(RolerightExample example);

    int deleteByExample(RolerightExample example);

    int deleteByPrimaryKey(Integer rmId);

    int insert(Roleright record);

    int insertSelective(Roleright record);

    List<Roleright> selectByExample(RolerightExample example);

    Roleright selectByPrimaryKey(Integer rmId);

    int updateByExampleSelective(@Param("record") Roleright record, @Param("example") RolerightExample example);

    int updateByExample(@Param("record") Roleright record, @Param("example") RolerightExample example);

    int updateByPrimaryKeySelective(Roleright record);

    int updateByPrimaryKey(Roleright record);

}
