package com.hbxy.se160105.portal.admin.app.service.impl;

import com.hbxy.se160105.portal.admin.app.mapper.AppMapper;
import com.hbxy.se160105.portal.admin.app.model.App;
import com.hbxy.se160105.portal.admin.app.model.AppExample;
import com.hbxy.se160105.portal.admin.app.service.AppService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class AppServicelmpl implements AppService {

    @Autowired
    private AppMapper appMapper;
    @Override
    public long countByExample(AppExample example){return appMapper.countByExample(example);}

    @Override
    public int deleteByExample(AppExample example){return appMapper.deleteByExample(example);}

    @Override
    public int deleteByPrimaryKey(Integer appId){return appMapper.deleteByPrimaryKey(appId);}

    @Override
    public  int insert(App record){return appMapper.insert(record);}

    @Override
    public  int insertSelective(App record){return appMapper.insertSelective(record);}

    @Override
    public  List<App> selectByExample(AppExample example){return appMapper.selectByExample(example);}

    @Override
    public App selectByPrimaryKey(Integer appId){return  appMapper.selectByPrimaryKey(appId);}

    @Override
    public  int updateByExampleSelective(@Param("record") App record, @Param("example") AppExample example){return appMapper.updateByExampleSelective(record,example);}

    @Override
    public  int updateByExample(@Param("record") App record, @Param("example") AppExample example){return appMapper.updateByExample(record,example);}

    @Override
    public  int updateByPrimaryKeySelective(App record){return appMapper.updateByPrimaryKeySelective(record);}

    @Override
    public  int updateByPrimaryKey(App record){return appMapper.updateByPrimaryKey(record);}
}
