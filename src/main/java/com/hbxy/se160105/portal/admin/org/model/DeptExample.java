package com.hbxy.se160105.portal.admin.org.model;

import java.util.ArrayList;
import java.util.List;

public class DeptExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public DeptExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andDeptIdIsNull() {
            addCriterion("dept_Id is null");
            return (Criteria) this;
        }

        public Criteria andDeptIdIsNotNull() {
            addCriterion("dept_Id is not null");
            return (Criteria) this;
        }

        public Criteria andDeptIdEqualTo(Integer value) {
            addCriterion("dept_Id =", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdNotEqualTo(Integer value) {
            addCriterion("dept_Id <>", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdGreaterThan(Integer value) {
            addCriterion("dept_Id >", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("dept_Id >=", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdLessThan(Integer value) {
            addCriterion("dept_Id <", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdLessThanOrEqualTo(Integer value) {
            addCriterion("dept_Id <=", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdIn(List<Integer> values) {
            addCriterion("dept_Id in", values, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdNotIn(List<Integer> values) {
            addCriterion("dept_Id not in", values, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdBetween(Integer value1, Integer value2) {
            addCriterion("dept_Id between", value1, value2, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdNotBetween(Integer value1, Integer value2) {
            addCriterion("dept_Id not between", value1, value2, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptNameIsNull() {
            addCriterion("dept_name is null");
            return (Criteria) this;
        }

        public Criteria andDeptNameIsNotNull() {
            addCriterion("dept_name is not null");
            return (Criteria) this;
        }

        public Criteria andDeptNameEqualTo(String value) {
            addCriterion("dept_name =", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameNotEqualTo(String value) {
            addCriterion("dept_name <>", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameGreaterThan(String value) {
            addCriterion("dept_name >", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameGreaterThanOrEqualTo(String value) {
            addCriterion("dept_name >=", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameLessThan(String value) {
            addCriterion("dept_name <", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameLessThanOrEqualTo(String value) {
            addCriterion("dept_name <=", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameLike(String value) {
            addCriterion("dept_name like", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameNotLike(String value) {
            addCriterion("dept_name not like", value, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameIn(List<String> values) {
            addCriterion("dept_name in", values, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameNotIn(List<String> values) {
            addCriterion("dept_name not in", values, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameBetween(String value1, String value2) {
            addCriterion("dept_name between", value1, value2, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptNameNotBetween(String value1, String value2) {
            addCriterion("dept_name not between", value1, value2, "deptName");
            return (Criteria) this;
        }

        public Criteria andDeptFatherIsNull() {
            addCriterion("dept_father is null");
            return (Criteria) this;
        }

        public Criteria andDeptFatherIsNotNull() {
            addCriterion("dept_father is not null");
            return (Criteria) this;
        }

        public Criteria andDeptFatherEqualTo(Integer value) {
            addCriterion("dept_father =", value, "deptFather");
            return (Criteria) this;
        }

        public Criteria andDeptFatherNotEqualTo(Integer value) {
            addCriterion("dept_father <>", value, "deptFather");
            return (Criteria) this;
        }

        public Criteria andDeptFatherGreaterThan(Integer value) {
            addCriterion("dept_father >", value, "deptFather");
            return (Criteria) this;
        }

        public Criteria andDeptFatherGreaterThanOrEqualTo(Integer value) {
            addCriterion("dept_father >=", value, "deptFather");
            return (Criteria) this;
        }

        public Criteria andDeptFatherLessThan(Integer value) {
            addCriterion("dept_father <", value, "deptFather");
            return (Criteria) this;
        }

        public Criteria andDeptFatherLessThanOrEqualTo(Integer value) {
            addCriterion("dept_father <=", value, "deptFather");
            return (Criteria) this;
        }

        public Criteria andDeptFatherIn(List<Integer> values) {
            addCriterion("dept_father in", values, "deptFather");
            return (Criteria) this;
        }

        public Criteria andDeptFatherNotIn(List<Integer> values) {
            addCriterion("dept_father not in", values, "deptFather");
            return (Criteria) this;
        }

        public Criteria andDeptFatherBetween(Integer value1, Integer value2) {
            addCriterion("dept_father between", value1, value2, "deptFather");
            return (Criteria) this;
        }

        public Criteria andDeptFatherNotBetween(Integer value1, Integer value2) {
            addCriterion("dept_father not between", value1, value2, "deptFather");
            return (Criteria) this;
        }

        public Criteria andDeptNumIsNull() {
            addCriterion("dept_num is null");
            return (Criteria) this;
        }

        public Criteria andDeptNumIsNotNull() {
            addCriterion("dept_num is not null");
            return (Criteria) this;
        }

        public Criteria andDeptNumEqualTo(String value) {
            addCriterion("dept_num =", value, "deptNum");
            return (Criteria) this;
        }

        public Criteria andDeptNumNotEqualTo(String value) {
            addCriterion("dept_num <>", value, "deptNum");
            return (Criteria) this;
        }

        public Criteria andDeptNumGreaterThan(String value) {
            addCriterion("dept_num >", value, "deptNum");
            return (Criteria) this;
        }

        public Criteria andDeptNumGreaterThanOrEqualTo(String value) {
            addCriterion("dept_num >=", value, "deptNum");
            return (Criteria) this;
        }

        public Criteria andDeptNumLessThan(String value) {
            addCriterion("dept_num <", value, "deptNum");
            return (Criteria) this;
        }

        public Criteria andDeptNumLessThanOrEqualTo(String value) {
            addCriterion("dept_num <=", value, "deptNum");
            return (Criteria) this;
        }

        public Criteria andDeptNumLike(String value) {
            addCriterion("dept_num like", value, "deptNum");
            return (Criteria) this;
        }

        public Criteria andDeptNumNotLike(String value) {
            addCriterion("dept_num not like", value, "deptNum");
            return (Criteria) this;
        }

        public Criteria andDeptNumIn(List<String> values) {
            addCriterion("dept_num in", values, "deptNum");
            return (Criteria) this;
        }

        public Criteria andDeptNumNotIn(List<String> values) {
            addCriterion("dept_num not in", values, "deptNum");
            return (Criteria) this;
        }

        public Criteria andDeptNumBetween(String value1, String value2) {
            addCriterion("dept_num between", value1, value2, "deptNum");
            return (Criteria) this;
        }

        public Criteria andDeptNumNotBetween(String value1, String value2) {
            addCriterion("dept_num not between", value1, value2, "deptNum");
            return (Criteria) this;
        }

        public Criteria andDeptSiteIsNull() {
            addCriterion("dept_site is null");
            return (Criteria) this;
        }

        public Criteria andDeptSiteIsNotNull() {
            addCriterion("dept_site is not null");
            return (Criteria) this;
        }

        public Criteria andDeptSiteEqualTo(String value) {
            addCriterion("dept_site =", value, "deptSite");
            return (Criteria) this;
        }

        public Criteria andDeptSiteNotEqualTo(String value) {
            addCriterion("dept_site <>", value, "deptSite");
            return (Criteria) this;
        }

        public Criteria andDeptSiteGreaterThan(String value) {
            addCriterion("dept_site >", value, "deptSite");
            return (Criteria) this;
        }

        public Criteria andDeptSiteGreaterThanOrEqualTo(String value) {
            addCriterion("dept_site >=", value, "deptSite");
            return (Criteria) this;
        }

        public Criteria andDeptSiteLessThan(String value) {
            addCriterion("dept_site <", value, "deptSite");
            return (Criteria) this;
        }

        public Criteria andDeptSiteLessThanOrEqualTo(String value) {
            addCriterion("dept_site <=", value, "deptSite");
            return (Criteria) this;
        }

        public Criteria andDeptSiteLike(String value) {
            addCriterion("dept_site like", value, "deptSite");
            return (Criteria) this;
        }

        public Criteria andDeptSiteNotLike(String value) {
            addCriterion("dept_site not like", value, "deptSite");
            return (Criteria) this;
        }

        public Criteria andDeptSiteIn(List<String> values) {
            addCriterion("dept_site in", values, "deptSite");
            return (Criteria) this;
        }

        public Criteria andDeptSiteNotIn(List<String> values) {
            addCriterion("dept_site not in", values, "deptSite");
            return (Criteria) this;
        }

        public Criteria andDeptSiteBetween(String value1, String value2) {
            addCriterion("dept_site between", value1, value2, "deptSite");
            return (Criteria) this;
        }

        public Criteria andDeptSiteNotBetween(String value1, String value2) {
            addCriterion("dept_site not between", value1, value2, "deptSite");
            return (Criteria) this;
        }

        public Criteria andDeptCreateIsNull() {
            addCriterion("dept_create is null");
            return (Criteria) this;
        }

        public Criteria andDeptCreateIsNotNull() {
            addCriterion("dept_create is not null");
            return (Criteria) this;
        }

        public Criteria andDeptCreateEqualTo(String value) {
            addCriterion("dept_create =", value, "deptCreate");
            return (Criteria) this;
        }

        public Criteria andDeptCreateNotEqualTo(String value) {
            addCriterion("dept_create <>", value, "deptCreate");
            return (Criteria) this;
        }

        public Criteria andDeptCreateGreaterThan(String value) {
            addCriterion("dept_create >", value, "deptCreate");
            return (Criteria) this;
        }

        public Criteria andDeptCreateGreaterThanOrEqualTo(String value) {
            addCriterion("dept_create >=", value, "deptCreate");
            return (Criteria) this;
        }

        public Criteria andDeptCreateLessThan(String value) {
            addCriterion("dept_create <", value, "deptCreate");
            return (Criteria) this;
        }

        public Criteria andDeptCreateLessThanOrEqualTo(String value) {
            addCriterion("dept_create <=", value, "deptCreate");
            return (Criteria) this;
        }

        public Criteria andDeptCreateLike(String value) {
            addCriterion("dept_create like", value, "deptCreate");
            return (Criteria) this;
        }

        public Criteria andDeptCreateNotLike(String value) {
            addCriterion("dept_create not like", value, "deptCreate");
            return (Criteria) this;
        }

        public Criteria andDeptCreateIn(List<String> values) {
            addCriterion("dept_create in", values, "deptCreate");
            return (Criteria) this;
        }

        public Criteria andDeptCreateNotIn(List<String> values) {
            addCriterion("dept_create not in", values, "deptCreate");
            return (Criteria) this;
        }

        public Criteria andDeptCreateBetween(String value1, String value2) {
            addCriterion("dept_create between", value1, value2, "deptCreate");
            return (Criteria) this;
        }

        public Criteria andDeptCreateNotBetween(String value1, String value2) {
            addCriterion("dept_create not between", value1, value2, "deptCreate");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}