package com.hbxy.se160105.portal.admin.org.controller;

import com.hbxy.se160105.portal.admin.org.model.Login;
import com.hbxy.se160105.portal.admin.org.model.LoginExample;
import com.hbxy.se160105.portal.admin.org.model.Person;
import com.hbxy.se160105.portal.admin.org.service.LoginService;
import com.hbxy.se160105.portal.admin.org.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import sun.rmi.runtime.Log;

@Controller
@RequestMapping("/admin")
public class LoginController {
    @Autowired
    LoginService loginService;
    @Autowired
    PersonService personService;
    //更改密码
    @RequestMapping("/toPersonEditPwd")
    public String toPersonEditPwd(String personId, Model model){
        if(null != personId && !"".equalsIgnoreCase(personId)){
            int i=Integer.parseInt(personId);
            Login login=loginService.selectByPrimaryKey(i);
            Person person=personService.selectByPrimaryKey(i);
            model.addAttribute("login",login);
            model.addAttribute("person",person);
        }
        return "admin/personeditpwd";
    }
    @RequestMapping("/personEditPwd")
    public String personEditPwd(Login login){
//        int id = login.getPersonId();
//        String pwd = login.getPassword();
        loginService.updateByPrimaryKey(login);
        return "redirect:toPersonList";
    }

}
