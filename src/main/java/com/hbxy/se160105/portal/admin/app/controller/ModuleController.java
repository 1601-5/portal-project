package com.hbxy.se160105.portal.admin.app.controller;

import com.hbxy.se160105.portal.admin.app.model.App;
import com.hbxy.se160105.portal.admin.app.model.AppExample;
import com.hbxy.se160105.portal.admin.app.model.Module;
import com.hbxy.se160105.portal.admin.app.model.ModuleExample;
import com.hbxy.se160105.portal.admin.app.service.AppService;
import com.hbxy.se160105.portal.admin.app.service.ModuleService;
import com.hbxy.se160105.portal.admin.right.model.RolerightExample;
import com.hbxy.se160105.portal.admin.right.service.RolerightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class ModuleController {
    @Autowired
    ModuleService moduleService;
    @Autowired
    AppService appService;
    @Autowired
    RolerightService rolerightService;
    @RequestMapping("/toModuleList")
    public String moduleList(Model model){
        ModuleExample empty=new ModuleExample();
        List<Module> moduleList =moduleService.selectByExample(empty);
        model.addAttribute("modulelist",moduleList);
        AppExample example=new AppExample();
        List<App> appList=appService.selectByExample(example);
        model.addAttribute("applist",appList);
        return "admin/modulelist";
    }
    //新增模型
    @RequestMapping("/toModuleAdd")
    public String toModuleAdd(Model model){
        AppExample empty=new AppExample();
        List<App> list =appService.selectByExample(empty);
        model.addAttribute("applist",list);
        return "admin/moduleadd";
    }

    @RequestMapping("/moduleAdd")
    public String moduleAdd(Module module){
        moduleService.insert(module);
        return "redirect:toModuleList";
    }
    //去到修改页面
    @RequestMapping("/toModuleEdit")
    public String toModuleEdit(String moduleId, Model model){
        if(null != moduleId&& !"".equalsIgnoreCase(moduleId)){
            int i=Integer.parseInt(moduleId);
            Module module=moduleService.selectByPrimaryKey(i);
            model.addAttribute("module",module);

            int appId=module.getAppId();
            App name = appService.selectByPrimaryKey(appId);
            model.addAttribute("app",name);
        }
        return "admin/moduleedit";
    }
    @RequestMapping("/moduleEdit")
    public String moduleEdit(Module module){
//        String  m =module.getModuleUrl();
        moduleService.updateByPrimaryKey(module);
        return "redirect:toModuleList";
    }

    @Transactional
    @RequestMapping("/moduleDel")
    public String addDel(Integer moduleId) {
        moduleService.deleteByPrimaryKey(moduleId);
        RolerightExample rolerightExample=new RolerightExample();
        rolerightExample.createCriteria().andModuleIdEqualTo(moduleId);
        rolerightService.deleteByExample(rolerightExample);
        return "redirect:toModuleList";
    }
}
