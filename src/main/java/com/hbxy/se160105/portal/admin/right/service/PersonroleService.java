package com.hbxy.se160105.portal.admin.right.service;

import com.hbxy.se160105.portal.admin.right.model.Personrole;
import com.hbxy.se160105.portal.admin.right.model.PersonroleExample;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PersonroleService {

    long countByExample(PersonroleExample example);

    int deleteByExample(PersonroleExample example);

    int deleteByPrimaryKey(Integer prId);

    int insert(Personrole record);

    int insertSelective(Personrole record);

    List<Personrole> selectByExample(PersonroleExample example);

    Personrole selectByPrimaryKey(Integer prId);

    int updateByExampleSelective(@Param("record") Personrole record, @Param("example") PersonroleExample example);

    int updateByExample(@Param("record") Personrole record, @Param("example") PersonroleExample example);

    int updateByPrimaryKeySelective(Personrole record);

    int updateByPrimaryKey(Personrole record);

}
