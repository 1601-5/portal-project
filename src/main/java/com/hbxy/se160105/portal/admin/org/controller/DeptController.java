package com.hbxy.se160105.portal.admin.org.controller;

import com.hbxy.se160105.portal.admin.org.model.Dept;
import com.hbxy.se160105.portal.admin.org.model.DeptExample;
import com.hbxy.se160105.portal.admin.org.service.DeptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class DeptController {
    @Autowired
    DeptService deptService;
    //查询所有组织
    @RequestMapping("/toDeptList")
    public String deptList(Model model) {
        DeptExample empty=new DeptExample();
        List<Dept> list =deptService.selectByExample(empty);
        model.addAttribute("deptlist",list);
        return "admin/deptlist";
    }
    //增加组织
    @RequestMapping("/toDeptAdd")
    public String toDeptAdd(Model model){
        DeptExample empty=new DeptExample();
        List<Dept> list =deptService.selectByExample(empty);
        model.addAttribute("deptlist",list);
        return "admin/deptadd";
    }
    @RequestMapping("/deptAdd")
    public String deptAdd(Dept dept){
        deptService.insert(dept);
        return "redirect:toDeptList";
    }

    //修改组织信息
    @RequestMapping("/toDeptEdit")
    public String toDeptEdit(String deptId, Model model){
        if(null != deptId && !"".equalsIgnoreCase(deptId)){
            int i=Integer.parseInt(deptId);
            Dept dept=deptService.selectByPrimaryKey(i);
            model.addAttribute("dept",dept);

            DeptExample empty=new DeptExample();
            List<Dept> list =deptService.selectByExample(empty);
            model.addAttribute("fatherlist",list);
        }
        return "admin/deptedit";
    }
    //修改页面,点击保存按钮后,保存信息
    @RequestMapping("/deptEdit")
    public String deptEdit(Dept dept){
        deptService.updateByPrimaryKey(dept);
        return "redirect:toDeptList";
    }

   //删除组织
    @Transactional
    @RequestMapping("/deptDel")
    public String deptDel(String deptId){
        int i=Integer.parseInt(deptId);
        deptService.deleteByPrimaryKey(i);
        //TODO:
        //2 删除子表或者关联表数据
        //2.1 构造一个deptId的查询条件 将其下级组织的上级组织改为其上级组织或置空
//        DeptExample example = new DeptExample();
//        example.createCriteria().andDeptFatherEqualTo(deptId);
//        deptService.updateByExample(dept);
        //2.2构造一个deptId的查询条件
        return "redirect:toDeptList";
    }
}
