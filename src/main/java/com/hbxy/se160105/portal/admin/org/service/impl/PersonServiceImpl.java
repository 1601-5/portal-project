package com.hbxy.se160105.portal.admin.org.service.impl;

import com.hbxy.se160105.portal.admin.org.mapper.PersonMapper;
import com.hbxy.se160105.portal.admin.org.model.Person;
import com.hbxy.se160105.portal.admin.org.model.PersonExample;
import com.hbxy.se160105.portal.admin.org.service.PersonService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PersonServiceImpl implements PersonService {
    @Autowired
    PersonMapper personMapper;
    @Override
    public long countByExample(PersonExample example){return personMapper.countByExample(example);}

    @Override
    public int deleteByExample(PersonExample example){return personMapper.deleteByExample(example);}

    @Override
    public int deleteByPrimaryKey(Integer DeptId){return personMapper.deleteByPrimaryKey(DeptId);}

    @Override
    public  int insert(Person record){return personMapper.insert(record);}

    @Override
    public  int insertSelective(Person record){return personMapper.insertSelective(record);}

    @Override
    public List<Person> selectByExample(PersonExample example){return personMapper.selectByExample(example);}

    @Override
    public Person selectByPrimaryKey(Integer DeptId){return  personMapper.selectByPrimaryKey(DeptId);}

    @Override
    public  int updateByExampleSelective(@Param("record") Person record, @Param("example") PersonExample example){return personMapper.updateByExampleSelective(record,example);}

    @Override
    public  int updateByExample(@Param("record") Person record, @Param("example") PersonExample example){return personMapper.updateByExample(record,example);}

    @Override
    public  int updateByPrimaryKeySelective(Person record){return personMapper.updateByPrimaryKeySelective(record);}

    @Override
    public  int updateByPrimaryKey(Person record){return personMapper.updateByPrimaryKey(record);}


}
