package com.hbxy.se160105.portal.admin.app.service;

import com.hbxy.se160105.portal.admin.app.model.Module;
import com.hbxy.se160105.portal.admin.app.model.ModuleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;


public interface ModuleService {
    long countByExample(ModuleExample example);

    int deleteByExample(ModuleExample example);

    int deleteByPrimaryKey(Integer moduleId);

    int insert(Module record);

    int insertSelective(Module record);

    List<Module> selectByExample(ModuleExample example);

    Module selectByPrimaryKey(Integer moduleId);

    int updateByExampleSelective(@Param("record") Module record, @Param("example") ModuleExample example);

    int updateByExample(@Param("record") Module record, @Param("example") ModuleExample example);

    int updateByPrimaryKeySelective(Module record);

    int updateByPrimaryKey(Module record);
}