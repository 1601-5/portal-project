package com.hbxy.se160105.portal.admin.right.controller;

import com.hbxy.se160105.portal.admin.app.model.Module;
import com.hbxy.se160105.portal.admin.app.model.ModuleExample;
import com.hbxy.se160105.portal.admin.app.service.ModuleService;
import com.hbxy.se160105.portal.admin.org.model.PersonExample;
import com.hbxy.se160105.portal.admin.right.model.*;
import com.hbxy.se160105.portal.admin.right.service.PersonroleService;
import com.hbxy.se160105.portal.admin.right.service.RoleService;
import com.hbxy.se160105.portal.admin.right.service.RolerightService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class RoleController {
    @Autowired
    RoleService roleService;
    @Autowired
    ModuleService moduleService;
    @Autowired
    RolerightService rolerightService;
    @Autowired
    PersonroleService personroleService;
    //查询所有角色
    @RequestMapping("/toRoleList")
    public String roleList(Model model) {
        RoleExample empty = new RoleExample();
        List<Role> list = roleService.selectByExample(empty);
        model.addAttribute("rolelist", list);
        return "admin/rolelist";
    }

    //增加角色
    @RequestMapping("/toRoleAdd")
    public String toRoleAdd(Model model){
        ModuleExample empty=new ModuleExample();
        List<Module> moduleList=moduleService.selectByExample(empty);
        model.addAttribute("modulelist",moduleList);
        return "admin/roleadd";
    }
    @Transactional
    @RequestMapping("/roleAdd")
    public String roleAdd(Role role, Integer[] moduleId){
        roleService.insert(role);
        int roleId=role.getRoleId();
        if(moduleId != null){
            for(int l=0;l<moduleId.length;l++){
                Roleright roleright = new Roleright();
                roleright.setRoleId(roleId);
                roleright.setModuleId(moduleId[l]);
                rolerightService.insert(roleright);
            }
        }

        return "redirect:toRoleList";
    }
    //修改角色信息
    @RequestMapping("/toRoleEdit")
    public String toRoleEdit(String roleId, Model model){
        if(null != roleId && !"".equalsIgnoreCase(roleId)){
            int i=Integer.parseInt(roleId);
            Role role=roleService.selectByPrimaryKey(i);
            model.addAttribute("role",role);

            ModuleExample empty=new ModuleExample();
            List<Module> moduleList=moduleService.selectByExample(empty);
            model.addAttribute("modulelist",moduleList);

            RolerightExample rolerightExample = new RolerightExample();
            rolerightExample.createCriteria().andRoleIdEqualTo(i);
            List<Roleright> rolerightList=rolerightService.selectByExample(rolerightExample);
            model.addAttribute("rolerightlist",rolerightList);
        }
        return "admin/roleedit";
    }
    //修改页面,点击保存按钮后,保存信息
    @Transactional
    @RequestMapping("/roleEdit")
    public String roleEdit(Role role,Integer[] moduleId){
        roleService.updateByPrimaryKey(role);
        int roleId=role.getRoleId();
        RolerightExample rolerightExample = new RolerightExample();
        rolerightExample.createCriteria().andRoleIdEqualTo(roleId);
        rolerightService.deleteByExample(rolerightExample);
        if(moduleId != null) {
            for(int l=0;l<moduleId.length;l++){
                Roleright roleright = new Roleright();
                roleright.setRoleId(roleId);
                roleright.setModuleId(moduleId[l]);
                rolerightService.insert(roleright);
            }
        }

        return "redirect:toRoleList";
    }

    //删除角色
    @Transactional
    @RequestMapping("/roleDel")
    public String roleDel(Integer roleId){
        roleService.deleteByPrimaryKey(roleId);
        //TODO:
        //2 删除子表或者关联表数据
        //2.1 构造一个roleId的查询条件 将其下级组织的上级组织改为其上级组织或置空
        PersonroleExample personroleExample=new PersonroleExample();
        personroleExample.createCriteria().andRoleIdEqualTo(roleId);
        personroleService.deleteByExample(personroleExample);
        //2.2构造一个roleId的查询条件
        RolerightExample rolerightExample=new RolerightExample();
        rolerightExample.createCriteria().andRoleIdEqualTo(roleId);
        rolerightService.deleteByExample(rolerightExample);
        return "redirect:toRoleList";
    }




}
