package com.hbxy.se160105.portal.admin.right.model;

import java.io.Serializable;

/**
 * role
 * @author 
 */
public class Role implements Serializable {
    private Integer roleId;

    private String roleName;

    private String roleNum;

    private String roleCreate;

    private static final long serialVersionUID = 1L;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getRoleNum() {
        return roleNum;
    }

    public void setRoleNum(String roleNum) {
        this.roleNum = roleNum;
    }

    public String getRoleCreate() {
        return roleCreate;
    }

    public void setRoleCreate(String roleCreate) {
        this.roleCreate = roleCreate;
    }
}