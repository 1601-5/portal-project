package com.hbxy.se160105.portal.admin.org.service.impl;

import com.hbxy.se160105.portal.admin.org.mapper.LoginMapper;
import com.hbxy.se160105.portal.admin.org.model.Login;
import com.hbxy.se160105.portal.admin.org.model.LoginExample;
import com.hbxy.se160105.portal.admin.org.service.LoginService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    private LoginMapper loginMapper;

    @Override
    public long countByExample(LoginExample example){return loginMapper.countByExample(example);}

    @Override
    public int deleteByExample(LoginExample example){return loginMapper.deleteByExample(example);}

    @Override
    public int deleteByPrimaryKey(Integer personId){return loginMapper.deleteByPrimaryKey(personId);}

    @Override
    public  int insert(Login record){return loginMapper.insert(record);}

    @Override
    public  int insertSelective(Login record){return loginMapper.insertSelective(record);}

    @Override
    public List<Login> selectByExample(LoginExample example){return loginMapper.selectByExample(example);}

    @Override
    public Login selectByPrimaryKey(Integer personId){return  loginMapper.selectByPrimaryKey(personId);}

    @Override
    public  int updateByExampleSelective(@Param("record") Login record, @Param("example") LoginExample example){return loginMapper.updateByExampleSelective(record,example);}

    @Override
    public  int updateByExample(@Param("record") Login record, @Param("example") LoginExample example){return loginMapper.updateByExample(record,example);}

    @Override
    public  int updateByPrimaryKeySelective(Login record){return loginMapper.updateByPrimaryKeySelective(record);}

    @Override
    public  int updateByPrimaryKey(Login record){return loginMapper.updateByPrimaryKey(record);}

}
