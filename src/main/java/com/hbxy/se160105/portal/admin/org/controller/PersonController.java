package com.hbxy.se160105.portal.admin.org.controller;

import com.hbxy.se160105.portal.admin.org.model.*;
import com.hbxy.se160105.portal.admin.org.service.DeptService;
import com.hbxy.se160105.portal.admin.org.service.LoginService;
import com.hbxy.se160105.portal.admin.org.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class PersonController {
    @Autowired
    PersonService personService;
    @Autowired
    DeptService deptService;
    @Autowired
    LoginService loginService;
    //查询所有人员
    @RequestMapping("/toPersonList")
    public String personList(Model model) {
        PersonExample personExample= new PersonExample();
        List<Person> personList=personService.selectByExample(personExample);
        model.addAttribute("personlist",personList);
        DeptExample deptExample=new DeptExample();
        List<Dept> deptList =deptService.selectByExample(deptExample);
        model.addAttribute("deptlist",deptList);
        LoginExample loginExample = new LoginExample();
        List<Login> loginList=loginService.selectByExample(loginExample);
        model.addAttribute("loginlist",loginList);
        return "admin/personlist";
    }

    //注册人员
    @RequestMapping("/toPersonAdd")
    public String toPersonAdd(Model model){
        DeptExample empty=new DeptExample();
        List<Dept> list =deptService.selectByExample(empty);
        model.addAttribute("deptlist",list);
        return "admin/personadd";
    }
    @Transactional
    @RequestMapping("/personAdd")
    public String personAdd(Person person,Login login,Model model){
        LoginExample loginExample=new LoginExample();
        loginExample.createCriteria().andUserIdEqualTo(login.getUserId());
        List<Login> isExist = loginService.selectByExample(loginExample);
        if(isExist.size()==0 && !login.getUserId().equals("")){
            personService.insert(person);
            loginService.insert(login);
            return "redirect:toPersonList";
        }
        DeptExample empty=new DeptExample();
        List<Dept> list =deptService.selectByExample(empty);
        model.addAttribute("deptlist",list);
        model.addAttribute("msg", "登录ID已存在，请重新录入！");
        return "admin/personadd";
    }
    //更改个人信息
    @RequestMapping("/toPersonEditInfo")
    public String toPersonEditInfo(String personId, Model model){
        if(null != personId && !"".equalsIgnoreCase(personId)){
            int i=Integer.parseInt(personId);
            Person person=personService.selectByPrimaryKey(i);
            Login login=loginService.selectByPrimaryKey(i);
            model.addAttribute("person",person);
            model.addAttribute("login",login);
        }
        return "admin/personeditinfo";
    }
    //修改页面,点击保存按钮后,保存信息
    @RequestMapping("/personEditInfo")
    public String personEditInfo(Person person,Login login){
        personService.updateByPrimaryKey(person);
//        loginService.updateByPrimaryKey(login);
        return "redirect:toPersonList";
    }

    // 调岗
    @RequestMapping("/toPersonEditDept")
    public String toPersonEditDept(String personId, Model model){
        if(null != personId && !"".equalsIgnoreCase(personId)){
            int i=Integer.parseInt(personId);
            Person person=personService.selectByPrimaryKey(i);
            model.addAttribute("person",person);

            DeptExample deptExample=new DeptExample();
            List<Dept> deptlist =deptService.selectByExample(deptExample);
            model.addAttribute("deptlist",deptlist);
        }
        return "admin/personeditdept";
    }

    //修改页面,点击保存按钮后,保存信息
    @RequestMapping("/personEdit")
    public String personEdit(Person person){
        personService.updateByPrimaryKey(person);
        return "redirect:toPersonList";
    }

    //删除人员
    @Transactional
    @RequestMapping("/personDel")
    public String personDel(String personId){
        int i=Integer.parseInt(personId);
        loginService.deleteByPrimaryKey(i);
        personService.deleteByPrimaryKey(i);

        return "redirect:toPersonList";
    }


}
