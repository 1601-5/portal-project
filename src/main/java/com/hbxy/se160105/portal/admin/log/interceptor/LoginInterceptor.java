package com.hbxy.se160105.portal.admin.log.interceptor;

import com.hbxy.se160105.portal.admin.org.model.Login;
import com.hbxy.se160105.portal.admin.util.RightManager;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LoginInterceptor implements HandlerInterceptor {
    // 不拦截 "/login" 请求
    private static final String[] IGNORE_URI = { "/login","/noright"};

    // 该方法将在 Controller 处理前进行调用
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object o) throws Exception {
        // flag 表示是否登录
        boolean flag = false;
        // 获取请求的 URL

        String url = request.getServletPath();
        String toUrl = request.getRequestURI();
        //保存要请求的url，登录完成后，继续跳转到该url
        request.getSession().setAttribute("toUrl",toUrl);

        for (String s : IGNORE_URI) {
            if (url.contains(s)) {
                flag = true;
                break;
            }
        }
        if (!flag) {
            // 获取 Session 并判断是否登录
            Login login =
                    (Login)request.getSession().getAttribute("USER_SESSION");
            if (login == null) {
                request.setAttribute("message", "Please log in first!");
                // 如果未登录，进行拦截，跳转到登录页面
                response.sendRedirect(request.getContextPath()+"/login");
            } else {
                if ("1".equalsIgnoreCase(login.getIsAdmin())) {
                    //系统管理员暂时不用鉴权
                    flag = true;
                }else{
                    //鉴权,判断请求的url是否在权限列表
                    RightManager rightManager =new RightManager();
                    if(!rightManager.hasRightByUrl(request)){
                        response.sendRedirect(request.getContextPath()+"/noright");
                    }else{
                        flag = true;
                    }
                }
            }
        }
        return flag;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}