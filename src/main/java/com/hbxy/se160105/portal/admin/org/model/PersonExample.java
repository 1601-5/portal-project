package com.hbxy.se160105.portal.admin.org.model;

import java.util.ArrayList;
import java.util.List;

public class PersonExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    private Integer limit;

    private Long offset;

    public PersonExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setOffset(Long offset) {
        this.offset = offset;
    }

    public Long getOffset() {
        return offset;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andPersonIdIsNull() {
            addCriterion("person_Id is null");
            return (Criteria) this;
        }

        public Criteria andPersonIdIsNotNull() {
            addCriterion("person_Id is not null");
            return (Criteria) this;
        }

        public Criteria andPersonIdEqualTo(Integer value) {
            addCriterion("person_Id =", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotEqualTo(Integer value) {
            addCriterion("person_Id <>", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThan(Integer value) {
            addCriterion("person_Id >", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("person_Id >=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThan(Integer value) {
            addCriterion("person_Id <", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdLessThanOrEqualTo(Integer value) {
            addCriterion("person_Id <=", value, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdIn(List<Integer> values) {
            addCriterion("person_Id in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotIn(List<Integer> values) {
            addCriterion("person_Id not in", values, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdBetween(Integer value1, Integer value2) {
            addCriterion("person_Id between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andPersonIdNotBetween(Integer value1, Integer value2) {
            addCriterion("person_Id not between", value1, value2, "personId");
            return (Criteria) this;
        }

        public Criteria andDeptIdIsNull() {
            addCriterion("dept_Id is null");
            return (Criteria) this;
        }

        public Criteria andDeptIdIsNotNull() {
            addCriterion("dept_Id is not null");
            return (Criteria) this;
        }

        public Criteria andDeptIdEqualTo(Integer value) {
            addCriterion("dept_Id =", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdNotEqualTo(Integer value) {
            addCriterion("dept_Id <>", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdGreaterThan(Integer value) {
            addCriterion("dept_Id >", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdGreaterThanOrEqualTo(Integer value) {
            addCriterion("dept_Id >=", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdLessThan(Integer value) {
            addCriterion("dept_Id <", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdLessThanOrEqualTo(Integer value) {
            addCriterion("dept_Id <=", value, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdIn(List<Integer> values) {
            addCriterion("dept_Id in", values, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdNotIn(List<Integer> values) {
            addCriterion("dept_Id not in", values, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdBetween(Integer value1, Integer value2) {
            addCriterion("dept_Id between", value1, value2, "deptId");
            return (Criteria) this;
        }

        public Criteria andDeptIdNotBetween(Integer value1, Integer value2) {
            addCriterion("dept_Id not between", value1, value2, "deptId");
            return (Criteria) this;
        }

        public Criteria andPersonNameIsNull() {
            addCriterion("person_name is null");
            return (Criteria) this;
        }

        public Criteria andPersonNameIsNotNull() {
            addCriterion("person_name is not null");
            return (Criteria) this;
        }

        public Criteria andPersonNameEqualTo(String value) {
            addCriterion("person_name =", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameNotEqualTo(String value) {
            addCriterion("person_name <>", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameGreaterThan(String value) {
            addCriterion("person_name >", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameGreaterThanOrEqualTo(String value) {
            addCriterion("person_name >=", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameLessThan(String value) {
            addCriterion("person_name <", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameLessThanOrEqualTo(String value) {
            addCriterion("person_name <=", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameLike(String value) {
            addCriterion("person_name like", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameNotLike(String value) {
            addCriterion("person_name not like", value, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameIn(List<String> values) {
            addCriterion("person_name in", values, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameNotIn(List<String> values) {
            addCriterion("person_name not in", values, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameBetween(String value1, String value2) {
            addCriterion("person_name between", value1, value2, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonNameNotBetween(String value1, String value2) {
            addCriterion("person_name not between", value1, value2, "personName");
            return (Criteria) this;
        }

        public Criteria andPersonTelIsNull() {
            addCriterion("person_tel is null");
            return (Criteria) this;
        }

        public Criteria andPersonTelIsNotNull() {
            addCriterion("person_tel is not null");
            return (Criteria) this;
        }

        public Criteria andPersonTelEqualTo(String value) {
            addCriterion("person_tel =", value, "personTel");
            return (Criteria) this;
        }

        public Criteria andPersonTelNotEqualTo(String value) {
            addCriterion("person_tel <>", value, "personTel");
            return (Criteria) this;
        }

        public Criteria andPersonTelGreaterThan(String value) {
            addCriterion("person_tel >", value, "personTel");
            return (Criteria) this;
        }

        public Criteria andPersonTelGreaterThanOrEqualTo(String value) {
            addCriterion("person_tel >=", value, "personTel");
            return (Criteria) this;
        }

        public Criteria andPersonTelLessThan(String value) {
            addCriterion("person_tel <", value, "personTel");
            return (Criteria) this;
        }

        public Criteria andPersonTelLessThanOrEqualTo(String value) {
            addCriterion("person_tel <=", value, "personTel");
            return (Criteria) this;
        }

        public Criteria andPersonTelLike(String value) {
            addCriterion("person_tel like", value, "personTel");
            return (Criteria) this;
        }

        public Criteria andPersonTelNotLike(String value) {
            addCriterion("person_tel not like", value, "personTel");
            return (Criteria) this;
        }

        public Criteria andPersonTelIn(List<String> values) {
            addCriterion("person_tel in", values, "personTel");
            return (Criteria) this;
        }

        public Criteria andPersonTelNotIn(List<String> values) {
            addCriterion("person_tel not in", values, "personTel");
            return (Criteria) this;
        }

        public Criteria andPersonTelBetween(String value1, String value2) {
            addCriterion("person_tel between", value1, value2, "personTel");
            return (Criteria) this;
        }

        public Criteria andPersonTelNotBetween(String value1, String value2) {
            addCriterion("person_tel not between", value1, value2, "personTel");
            return (Criteria) this;
        }

        public Criteria andPersonSexIsNull() {
            addCriterion("person_sex is null");
            return (Criteria) this;
        }

        public Criteria andPersonSexIsNotNull() {
            addCriterion("person_sex is not null");
            return (Criteria) this;
        }

        public Criteria andPersonSexEqualTo(String value) {
            addCriterion("person_sex =", value, "personSex");
            return (Criteria) this;
        }

        public Criteria andPersonSexNotEqualTo(String value) {
            addCriterion("person_sex <>", value, "personSex");
            return (Criteria) this;
        }

        public Criteria andPersonSexGreaterThan(String value) {
            addCriterion("person_sex >", value, "personSex");
            return (Criteria) this;
        }

        public Criteria andPersonSexGreaterThanOrEqualTo(String value) {
            addCriterion("person_sex >=", value, "personSex");
            return (Criteria) this;
        }

        public Criteria andPersonSexLessThan(String value) {
            addCriterion("person_sex <", value, "personSex");
            return (Criteria) this;
        }

        public Criteria andPersonSexLessThanOrEqualTo(String value) {
            addCriterion("person_sex <=", value, "personSex");
            return (Criteria) this;
        }

        public Criteria andPersonSexLike(String value) {
            addCriterion("person_sex like", value, "personSex");
            return (Criteria) this;
        }

        public Criteria andPersonSexNotLike(String value) {
            addCriterion("person_sex not like", value, "personSex");
            return (Criteria) this;
        }

        public Criteria andPersonSexIn(List<String> values) {
            addCriterion("person_sex in", values, "personSex");
            return (Criteria) this;
        }

        public Criteria andPersonSexNotIn(List<String> values) {
            addCriterion("person_sex not in", values, "personSex");
            return (Criteria) this;
        }

        public Criteria andPersonSexBetween(String value1, String value2) {
            addCriterion("person_sex between", value1, value2, "personSex");
            return (Criteria) this;
        }

        public Criteria andPersonSexNotBetween(String value1, String value2) {
            addCriterion("person_sex not between", value1, value2, "personSex");
            return (Criteria) this;
        }

        public Criteria andPersonEntertimeIsNull() {
            addCriterion("person_entertime is null");
            return (Criteria) this;
        }

        public Criteria andPersonEntertimeIsNotNull() {
            addCriterion("person_entertime is not null");
            return (Criteria) this;
        }

        public Criteria andPersonEntertimeEqualTo(String value) {
            addCriterion("person_entertime =", value, "personEntertime");
            return (Criteria) this;
        }

        public Criteria andPersonEntertimeNotEqualTo(String value) {
            addCriterion("person_entertime <>", value, "personEntertime");
            return (Criteria) this;
        }

        public Criteria andPersonEntertimeGreaterThan(String value) {
            addCriterion("person_entertime >", value, "personEntertime");
            return (Criteria) this;
        }

        public Criteria andPersonEntertimeGreaterThanOrEqualTo(String value) {
            addCriterion("person_entertime >=", value, "personEntertime");
            return (Criteria) this;
        }

        public Criteria andPersonEntertimeLessThan(String value) {
            addCriterion("person_entertime <", value, "personEntertime");
            return (Criteria) this;
        }

        public Criteria andPersonEntertimeLessThanOrEqualTo(String value) {
            addCriterion("person_entertime <=", value, "personEntertime");
            return (Criteria) this;
        }

        public Criteria andPersonEntertimeLike(String value) {
            addCriterion("person_entertime like", value, "personEntertime");
            return (Criteria) this;
        }

        public Criteria andPersonEntertimeNotLike(String value) {
            addCriterion("person_entertime not like", value, "personEntertime");
            return (Criteria) this;
        }

        public Criteria andPersonEntertimeIn(List<String> values) {
            addCriterion("person_entertime in", values, "personEntertime");
            return (Criteria) this;
        }

        public Criteria andPersonEntertimeNotIn(List<String> values) {
            addCriterion("person_entertime not in", values, "personEntertime");
            return (Criteria) this;
        }

        public Criteria andPersonEntertimeBetween(String value1, String value2) {
            addCriterion("person_entertime between", value1, value2, "personEntertime");
            return (Criteria) this;
        }

        public Criteria andPersonEntertimeNotBetween(String value1, String value2) {
            addCriterion("person_entertime not between", value1, value2, "personEntertime");
            return (Criteria) this;
        }

        public Criteria andPersonPayIsNull() {
            addCriterion("person_pay is null");
            return (Criteria) this;
        }

        public Criteria andPersonPayIsNotNull() {
            addCriterion("person_pay is not null");
            return (Criteria) this;
        }

        public Criteria andPersonPayEqualTo(String value) {
            addCriterion("person_pay =", value, "personPay");
            return (Criteria) this;
        }

        public Criteria andPersonPayNotEqualTo(String value) {
            addCriterion("person_pay <>", value, "personPay");
            return (Criteria) this;
        }

        public Criteria andPersonPayGreaterThan(String value) {
            addCriterion("person_pay >", value, "personPay");
            return (Criteria) this;
        }

        public Criteria andPersonPayGreaterThanOrEqualTo(String value) {
            addCriterion("person_pay >=", value, "personPay");
            return (Criteria) this;
        }

        public Criteria andPersonPayLessThan(String value) {
            addCriterion("person_pay <", value, "personPay");
            return (Criteria) this;
        }

        public Criteria andPersonPayLessThanOrEqualTo(String value) {
            addCriterion("person_pay <=", value, "personPay");
            return (Criteria) this;
        }

        public Criteria andPersonPayLike(String value) {
            addCriterion("person_pay like", value, "personPay");
            return (Criteria) this;
        }

        public Criteria andPersonPayNotLike(String value) {
            addCriterion("person_pay not like", value, "personPay");
            return (Criteria) this;
        }

        public Criteria andPersonPayIn(List<String> values) {
            addCriterion("person_pay in", values, "personPay");
            return (Criteria) this;
        }

        public Criteria andPersonPayNotIn(List<String> values) {
            addCriterion("person_pay not in", values, "personPay");
            return (Criteria) this;
        }

        public Criteria andPersonPayBetween(String value1, String value2) {
            addCriterion("person_pay between", value1, value2, "personPay");
            return (Criteria) this;
        }

        public Criteria andPersonPayNotBetween(String value1, String value2) {
            addCriterion("person_pay not between", value1, value2, "personPay");
            return (Criteria) this;
        }
    }

    /**
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}