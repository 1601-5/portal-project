package com.hbxy.se160105.portal.admin.right.service.impl;

import com.hbxy.se160105.portal.admin.right.mapper.PersonroleMapper;
import com.hbxy.se160105.portal.admin.right.model.Personrole;
import com.hbxy.se160105.portal.admin.right.model.PersonroleExample;
import com.hbxy.se160105.portal.admin.right.service.PersonroleService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PersonroleServiceImpl implements PersonroleService {
    @Autowired
    PersonroleMapper personroleMapper;
    @Override
    public long countByExample(PersonroleExample example){return personroleMapper.countByExample(example);}

    @Override
    public int deleteByExample(PersonroleExample example){return personroleMapper.deleteByExample(example);}

    @Override
    public int deleteByPrimaryKey(Integer RoleId){return personroleMapper.deleteByPrimaryKey(RoleId);}

    @Override
    public  int insert(Personrole record){return personroleMapper.insert(record);}

    @Override
    public  int insertSelective(Personrole record){return personroleMapper.insertSelective(record);}

    @Override
    public List<Personrole> selectByExample(PersonroleExample example){return personroleMapper.selectByExample(example);}

    @Override
    public Personrole selectByPrimaryKey(Integer RoleId){return  personroleMapper.selectByPrimaryKey(RoleId);}

    @Override
    public  int updateByExampleSelective(@Param("record") Personrole record, @Param("example") PersonroleExample example){return personroleMapper.updateByExampleSelective(record,example);}

    @Override
    public  int updateByExample(@Param("record") Personrole record, @Param("example") PersonroleExample example){return personroleMapper.updateByExample(record,example);}

    @Override
    public  int updateByPrimaryKeySelective(Personrole record){return personroleMapper.updateByPrimaryKeySelective(record);}

    @Override
    public  int updateByPrimaryKey(Personrole record){return personroleMapper.updateByPrimaryKey(record);}
}
