package com.hbxy.se160105.portal.admin.org.model;

import java.io.Serializable;

public class Dept implements Serializable {
    private Integer deptId;

    private String deptName;

    private Integer deptFather;

    private String deptNum;

    private String deptSite;

    private String deptCreate;

    private static final long serialVersionUID = 1L;

    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Integer getDeptFather() {
        return deptFather;
    }

    public void setDeptFather(Integer deptFather) {
        this.deptFather = deptFather;
    }

    public String getDeptNum() {
        return deptNum;
    }

    public void setDeptNum(String deptNum) {
        this.deptNum = deptNum;
    }

    public String getDeptSite() {
        return deptSite;
    }

    public void setDeptSite(String deptSite) {
        this.deptSite = deptSite;
    }

    public String getDeptCreate() {
        return deptCreate;
    }

    public void setDeptCreate(String deptCreate) {
        this.deptCreate = deptCreate;
    }
}