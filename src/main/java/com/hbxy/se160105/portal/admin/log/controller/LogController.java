package com.hbxy.se160105.portal.admin.log.controller;

import com.hbxy.se160105.portal.admin.app.model.App;
import com.hbxy.se160105.portal.admin.app.model.AppExample;
import com.hbxy.se160105.portal.admin.app.model.Module;
import com.hbxy.se160105.portal.admin.app.model.ModuleExample;
import com.hbxy.se160105.portal.admin.app.service.AppService;
import com.hbxy.se160105.portal.admin.app.service.ModuleService;
import com.hbxy.se160105.portal.admin.org.model.Login;
import com.hbxy.se160105.portal.admin.org.model.LoginExample;
import com.hbxy.se160105.portal.admin.org.model.Person;
import com.hbxy.se160105.portal.admin.org.model.PersonExample;
import com.hbxy.se160105.portal.admin.org.service.LoginService;
import com.hbxy.se160105.portal.admin.right.model.*;
import com.hbxy.se160105.portal.admin.right.service.PersonroleService;
import com.hbxy.se160105.portal.admin.right.service.RoleService;
import com.hbxy.se160105.portal.admin.right.service.RolerightService;
import com.hbxy.se160105.portal.admin.util.UserLoginUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

@Controller
public class LogController {
    @Autowired
    LoginService loginService;
    @Autowired
    RolerightService rolerightService;
    @Autowired
    PersonroleService personroleService;
    @Autowired
    RoleService roleService;
    @Autowired
    AppService appService;
    @Autowired
    ModuleService moduleService;

    //系统首页
//    @RequestMapping("/")
//    public String index(Model model, HttpServletResponse response) {
//        return "/index";
//    }
    /**
     * 向用户登录页面/login.jsp跳转
     */
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String toLogin() {
        return "login";
    }
    /**
     * 用户登录
     */
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String login(Login login, Model model, HttpServletRequest request) {
        // 获取用户名和密码
        String userLoginId = login.getUserId();
        String password = login.getPassword();

        if (userLoginId != null && password != null) {
            //根据登录ID获取登录User
            LoginExample userIdExample = new LoginExample();
            userIdExample.createCriteria().andUserIdEqualTo(userLoginId);
            List<Login> users = loginService.selectByExample(userIdExample);
            if(users != null){
                Login user=users.get(0);

                // 从数据库中获取用户名和密码后进行判断
                if(!user.getPassword().equals(password)){
                    model.addAttribute("msg", "用户名或密码错误，请重新登录！");
                    return "login";
                }
                // 将用户对象添加到Session
                request.getSession().setAttribute("USER_SESSION",user);
                //构造权限控制列表

                //根据人员ID查询角色ID
                int personId = user.getPersonId();
                PersonroleExample personroleExample = new PersonroleExample();
                personroleExample.createCriteria().andPersonIdEqualTo(personId);
                List<Personrole> personroles = personroleService.selectByExample(personroleExample);

                if(personroles.size()>0){
                    List<Integer> roleIds = new ArrayList<>();
                    for(Personrole pr : personroles){
                        roleIds.add(pr.getRoleId());
                    }

                    RolerightExample rolerightExample = new RolerightExample();//角色模块表 Roleright
                    RolerightExample.Criteria criteria = rolerightExample.createCriteria();
                    criteria.andRoleIdIn(roleIds);//设置查询条件
                    //获得角色权限
                    List<Roleright> rolerights = rolerightService.selectByExample(rolerightExample);

                    if(rolerights.size()>0){
                        List<Integer> moduleIds = new ArrayList<>();
                        for (Roleright rr : rolerights) {
                            moduleIds.add(rr.getModuleId());
                        }
                        ModuleExample moduleExample = new ModuleExample();
                        //设置查询条件
                        moduleExample.createCriteria().andModuleIdIn(moduleIds);
                        //获得模块权限
                        List<Module> modules = moduleService.selectByExample(moduleExample);

                        //构造权限访问列表(即具有可访问的模块URL),
                        //放进一个hashset,并放进用户当前session中,以便在拦截器中做为鉴权依据
                        HashSet<String> rightSet = new HashSet<>();
                        for (Module res : modules) {
                            if (!"".equalsIgnoreCase(res.getModuleUrl())) {
                                rightSet.add(res.getModuleUrl());
                            }
                        }
                        Login userlogin = UserLoginUtil.getUserLogin(request);
                        userlogin.setRightSet(rightSet);
                        //更新Session
                        request.getSession().setAttribute("USER_SESSION", user);
                    }

                }

                if ("1".equalsIgnoreCase(user.getIsAdmin())) {
                    return "redirect:admin/toHome";
                } else {
                    return "redirect:toHome";
                }
            }
        }
        model.addAttribute("msg", "用户名或密码错误，请重新登录！");
        return "login";
    }


    /**
     * 退出登录
     */
    @RequestMapping("/logout")
    public String logout(HttpSession session) {
        // 清除Session
        session.invalidate();
        // 重定向到登录页面的跳转方法
        return "redirect:login";
    }
    //admin系统首页
    @RequestMapping("/admin/toHome")
    public String home(Model model, HttpServletResponse response) {
        return "/admin/home";
    }
    //
    @RequestMapping("/toHome")
    public String toIndex(Model model, HttpServletRequest request) {
        AppExample appExample=new AppExample();
        ModuleExample moduleExample=new ModuleExample();
        List<App> appList=appService.selectByExample(appExample);
        List<Module>moduleList=moduleService.selectByExample(moduleExample);
        model.addAttribute("appList", appList);
        model.addAttribute("moduleList", moduleList);
        return "/home";
    }

    @RequestMapping("/noright")
    public String noright() {
        return "/noright";
    }

//    @RequestMapping("/toUserInfo")
//    public String toUserInfo(Model model) {
//
//        return "/userinfo";
//    }

}

