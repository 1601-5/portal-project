package com.hbxy.se160105.portal.admin.util;

import com.hbxy.se160105.portal.admin.org.model.Login;

import javax.servlet.http.HttpServletRequest;

public class UserLoginUtil {
    public final static Login getUserLogin(HttpServletRequest request){


        Login login = new Login();
        if(null != request.getSession().getAttribute("USER_SESSION")){
            login = (Login)request.getSession().getAttribute("USER_SESSION");
        }else{
            login.setUserId("admin");
        }


        return login;
    }
}
