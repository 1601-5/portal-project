package com.hbxy.se160105.portal.admin.right.service.impl;

import com.hbxy.se160105.portal.admin.right.mapper.RoleMapper;
import com.hbxy.se160105.portal.admin.right.model.Role;
import com.hbxy.se160105.portal.admin.right.model.RoleExample;
import com.hbxy.se160105.portal.admin.right.service.RoleService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RoleServiceImpl implements RoleService {
    @Autowired
    RoleMapper roleMapper;
    @Override
    public long countByExample(RoleExample example){return roleMapper.countByExample(example);}

    @Override
    public int deleteByExample(RoleExample example){return roleMapper.deleteByExample(example);}

    @Override
    public int deleteByPrimaryKey(Integer RoleId){return roleMapper.deleteByPrimaryKey(RoleId);}

    @Override
    public  int insert(Role record){return roleMapper.insert(record);}

    @Override
    public  int insertSelective(Role record){return roleMapper.insertSelective(record);}

    @Override
    public List<Role> selectByExample(RoleExample example){return roleMapper.selectByExample(example);}

    @Override
    public Role selectByPrimaryKey(Integer RoleId){return  roleMapper.selectByPrimaryKey(RoleId);}

    @Override
    public  int updateByExampleSelective(@Param("record") Role record, @Param("example") RoleExample example){return roleMapper.updateByExampleSelective(record,example);}

    @Override
    public  int updateByExample(@Param("record") Role record, @Param("example") RoleExample example){return roleMapper.updateByExample(record,example);}

    @Override
    public  int updateByPrimaryKeySelective(Role record){return roleMapper.updateByPrimaryKeySelective(record);}

    @Override
    public  int updateByPrimaryKey(Role record){return roleMapper.updateByPrimaryKey(record);}

}
