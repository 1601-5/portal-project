package com.hbxy.se160105.portal.admin.org.service.impl;

import com.hbxy.se160105.portal.admin.org.mapper.DeptMapper;
import com.hbxy.se160105.portal.admin.org.model.Dept;
import com.hbxy.se160105.portal.admin.org.model.DeptExample;
import com.hbxy.se160105.portal.admin.org.service.DeptService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class DeptServiceImpl implements DeptService {
    @Autowired
    private DeptMapper deptMapper;
    @Override
    public long countByExample(DeptExample example){return deptMapper.countByExample(example);}

    @Override
    public int deleteByExample(DeptExample example){return deptMapper.deleteByExample(example);}

    @Override
    public int deleteByPrimaryKey(Integer DeptId){return deptMapper.deleteByPrimaryKey(DeptId);}

    @Override
    public  int insert(Dept record){return deptMapper.insert(record);}

    @Override
    public  int insertSelective(Dept record){return deptMapper.insertSelective(record);}

    @Override
    public List<Dept> selectByExample(DeptExample example){return deptMapper.selectByExample(example);}

    @Override
    public Dept selectByPrimaryKey(Integer DeptId){return  deptMapper.selectByPrimaryKey(DeptId);}

    @Override
    public  int updateByExampleSelective(@Param("record") Dept record, @Param("example") DeptExample example){return deptMapper.updateByExampleSelective(record,example);}

    @Override
    public  int updateByExample(@Param("record") Dept record, @Param("example") DeptExample example){return deptMapper.updateByExample(record,example);}

    @Override
    public  int updateByPrimaryKeySelective(Dept record){return deptMapper.updateByPrimaryKeySelective(record);}

    @Override
    public  int updateByPrimaryKey(Dept record){return deptMapper.updateByPrimaryKey(record);}

}
