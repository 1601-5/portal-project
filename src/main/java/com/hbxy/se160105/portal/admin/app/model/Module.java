package com.hbxy.se160105.portal.admin.app.model;

import java.io.Serializable;

/**
 * module
 * @author 
 */
public class Module implements Serializable {
    private Integer moduleId;

    private Integer appId;

    private String moduleName;

    private String moduleNum;

    private String moduleUrl;

    private String moduleCreate;

    private static final long serialVersionUID = 1L;

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public Integer getAppId() {
        return appId;
    }

    public void setAppId(Integer appId) {
        this.appId = appId;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    public String getModuleNum() {
        return moduleNum;
    }

    public void setModuleNum(String moduleNum) {
        this.moduleNum = moduleNum;
    }

    public String getModuleUrl() {
        return moduleUrl;
    }

    public void setModuleUrl(String moduleUrl) {
        this.moduleUrl = moduleUrl;
    }

    public String getModuleCreate() {
        return moduleCreate;
    }

    public void setModuleCreate(String moduleCreate) {
        this.moduleCreate = moduleCreate;
    }
}