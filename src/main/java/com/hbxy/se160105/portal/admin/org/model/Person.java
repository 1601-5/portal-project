package com.hbxy.se160105.portal.admin.org.model;

import java.io.Serializable;

/**
 * person
 * @author 
 */
public class Person implements Serializable {
    private Integer personId;

    private Integer deptId;

    private String personName;

    private String personTel;

    private String personSex;

    private String personEntertime;

    private String personPay;

    private static final long serialVersionUID = 1L;

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Integer getDeptId() {
        return deptId;
    }

    public void setDeptId(Integer deptId) {
        this.deptId = deptId;
    }

    public String getPersonName() {
        return personName;
    }

    public void setPersonName(String personName) {
        this.personName = personName;
    }

    public String getPersonTel() {
        return personTel;
    }

    public void setPersonTel(String personTel) {
        this.personTel = personTel;
    }

    public String getPersonSex() {
        return personSex;
    }

    public void setPersonSex(String personSex) {
        this.personSex = personSex;
    }

    public String getPersonEntertime() {
        return personEntertime;
    }

    public void setPersonEntertime(String personEntertime) {
        this.personEntertime = personEntertime;
    }

    public String getPersonPay() {
        return personPay;
    }

    public void setPersonPay(String personPay) {
        this.personPay = personPay;
    }
}