package com.hbxy.se160105.portal.admin.org.model;

import java.io.Serializable;
import java.util.HashSet;

/**
 * login
 * @author 
 */
public class Login implements Serializable {

    //权限控制列表
    private HashSet rightSet;

    private Integer personId;

    private String userId;

    private String password;

    private String isAdmin;

    public HashSet getRightSet() {
        return rightSet;
    }

    public void setRightSet(HashSet rightSet) {
        this.rightSet = rightSet;
    }

    private static final long serialVersionUID = 1L;

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getIsAdmin() {
        return isAdmin;
    }

    public void setIsAdmin(String isAdmin) {
        this.isAdmin = isAdmin;
    }
}