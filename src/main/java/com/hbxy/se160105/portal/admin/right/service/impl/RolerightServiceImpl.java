package com.hbxy.se160105.portal.admin.right.service.impl;

import com.hbxy.se160105.portal.admin.right.mapper.RolerightMapper;
import com.hbxy.se160105.portal.admin.right.model.Roleright;
import com.hbxy.se160105.portal.admin.right.model.RolerightExample;
import com.hbxy.se160105.portal.admin.right.service.RolerightService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class RolerightServiceImpl implements RolerightService {
    @Autowired
    RolerightMapper rolerightMapper;
    @Override
    public long countByExample(RolerightExample example){return rolerightMapper.countByExample(example);}

    @Override
    public int deleteByExample(RolerightExample example){return rolerightMapper.deleteByExample(example);}

    @Override
    public int deleteByPrimaryKey(Integer RoleId){return rolerightMapper.deleteByPrimaryKey(RoleId);}

    @Override
    public  int insert(Roleright record){return rolerightMapper.insert(record);}

    @Override
    public  int insertSelective(Roleright record){return rolerightMapper.insertSelective(record);}

    @Override
    public List<Roleright> selectByExample(RolerightExample example){return rolerightMapper.selectByExample(example);}

    @Override
    public Roleright selectByPrimaryKey(Integer RoleId){return  rolerightMapper.selectByPrimaryKey(RoleId);}

    @Override
    public  int updateByExampleSelective(@Param("record") Roleright record, @Param("example") RolerightExample example){return rolerightMapper.updateByExampleSelective(record,example);}

    @Override
    public  int updateByExample(@Param("record") Roleright record, @Param("example") RolerightExample example){return rolerightMapper.updateByExample(record,example);}

    @Override
    public  int updateByPrimaryKeySelective(Roleright record){return rolerightMapper.updateByPrimaryKeySelective(record);}

    @Override
    public  int updateByPrimaryKey(Roleright record){return rolerightMapper.updateByPrimaryKey(record);}
}
