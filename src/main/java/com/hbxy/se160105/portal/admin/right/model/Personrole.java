package com.hbxy.se160105.portal.admin.right.model;

import java.io.Serializable;

/**
 * personrole
 * @author 
 */
public class Personrole implements Serializable {
    private Integer prId;

    private Integer personId;

    private Integer roleId;

    private static final long serialVersionUID = 1L;

    public Integer getPrId() {
        return prId;
    }

    public void setPrId(Integer prId) {
        this.prId = prId;
    }

    public Integer getPersonId() {
        return personId;
    }

    public void setPersonId(Integer personId) {
        this.personId = personId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
}