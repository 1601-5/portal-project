package com.hbxy.se160105.portal.admin.right.controller;

import com.hbxy.se160105.portal.admin.org.model.Login;
import com.hbxy.se160105.portal.admin.org.model.LoginExample;
import com.hbxy.se160105.portal.admin.org.model.Person;
import com.hbxy.se160105.portal.admin.org.model.PersonExample;
import com.hbxy.se160105.portal.admin.org.service.LoginService;
import com.hbxy.se160105.portal.admin.org.service.PersonService;
import com.hbxy.se160105.portal.admin.right.model.Personrole;
import com.hbxy.se160105.portal.admin.right.model.PersonroleExample;
import com.hbxy.se160105.portal.admin.right.model.Role;
import com.hbxy.se160105.portal.admin.right.model.RoleExample;
import com.hbxy.se160105.portal.admin.right.service.PersonroleService;
import com.hbxy.se160105.portal.admin.right.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class PersonroleController {
    @Autowired
    PersonroleService personroleService;
    @Autowired
    PersonService personService;
    @Autowired
    RoleService roleService;
    @Autowired
    LoginService loginService;
    //跳转到授权页面
    @RequestMapping("/toRoleMsg")
    public String toRoleMsg(Model model) {
        return "admin/rolemsg";
    }
    @RequestMapping("/toRoleMsg2")
    public String toRoleMsg2(Model model) {
        model.addAttribute("msg", "登录ID不存在，请重新录入！");
        return "admin/rolemsg";
    }
    //查询所有角色
    @RequestMapping("/toRoleQuery")
    public String toRoleQuery(String userId, Model model) {
        LoginExample loginExample=new LoginExample();
        loginExample.createCriteria().andUserIdEqualTo(userId);
        List<Login> logins=loginService.selectByExample(loginExample);

        if(logins.size()==0){
            return "redirect:toRoleMsg2";
        }

        boolean isExist=true;
        Login login=logins.get(0);
        int personId = login.getPersonId();
        model.addAttribute("isExist",isExist);
        PersonExample personExample = new PersonExample();
        Person person = personService.selectByPrimaryKey(personId);
        model.addAttribute("person", person);
        RoleExample empty = new RoleExample();
        List<Role> list = roleService.selectByExample(empty);
        model.addAttribute("rolelist", list);
        PersonroleExample personroleExample = new PersonroleExample();
        personroleExample.createCriteria().andPersonIdEqualTo(personId);
        List<Personrole> personroleList = personroleService.selectByExample(personroleExample);
        model.addAttribute("persomrolelist", personroleList);
        return "admin/rolemsg";
    }

    @Transactional
    @RequestMapping("/RoleMsg")
    public String roleMsg(Personrole personrole, Integer[] roleId) {
        int personId = personrole.getPersonId();
        PersonroleExample personroleExample = new PersonroleExample();
        personroleExample.createCriteria().andPersonIdEqualTo(personId);
        personroleService.deleteByExample(personroleExample);
        if (roleId != null) {
            for (int l = 0; l < roleId.length; l++) {
                Personrole personrole1 = new Personrole();
                personrole.setPersonId(personId);
                personrole.setRoleId(roleId[l]);
                personroleService.insert(personrole);
            }
        }

        return "redirect:toRoleMsg";
    }

}
