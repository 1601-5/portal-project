package com.hbxy.se160105.portal.admin.app.controller;

import com.hbxy.se160105.portal.admin.app.model.App;
import com.hbxy.se160105.portal.admin.app.model.AppExample;
import com.hbxy.se160105.portal.admin.app.model.ModuleExample;
import com.hbxy.se160105.portal.admin.app.service.ModuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import com.hbxy.se160105.portal.admin.app.service.AppService;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AppController {
    @Autowired
    AppService appService;
    @Autowired
    ModuleService moduleService;
    //查询所有应用
    @RequestMapping("/toAppList")
    public String appList(Model model) {
        AppExample empty=new AppExample();
        List<App> list =appService.selectByExample(empty);
        model.addAttribute("applist",list);
        return "admin/applist";
    }
    //增加应用
    @RequestMapping("/toAppAdd")
    public String toAppAdd(Model model){
        return "admin/appadd";
    }
    @RequestMapping("/appAdd")
    public String appAdd(App app){
        appService.insert(app);
        return "redirect:toAppList";
    }
    //转调修改页面
    @RequestMapping("/toAppEdit")
    public String toAppEdit(String appId, Model model){
        if(null != appId && !"".equalsIgnoreCase(appId)){
            int i=Integer.parseInt(appId);
            App app=appService.selectByPrimaryKey(i);
            model.addAttribute("app",app);
        }
        return "admin/appedit";
    }
    //修改页面,点击保存按钮后,保存信息
    @RequestMapping("/appEdit")
    public String appEdit(App app){
        appService.updateByPrimaryKey(app);
        return "redirect:toAppList";
    }
    @Transactional
    @RequestMapping("/appDel")
    public String addDel(String appId){
        int i=Integer.parseInt(appId);
        appService.deleteByPrimaryKey(i);
        //TODO:
        //2 删除子表或者关联表数据
        //2.1 构造一个appId的查询条件
        ModuleExample example = new ModuleExample();
        example.createCriteria().andAppIdEqualTo(i);
        moduleService.deleteByExample(example);
        return "redirect:toAppList";
    }
}
