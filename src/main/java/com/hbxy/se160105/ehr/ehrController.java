package com.hbxy.se160105.ehr;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping("/app")
public class ehrController {
    @RequestMapping("/test1")
    public String test1(Model model, HttpServletResponse response) {
        return "app/test1";
    }
    @RequestMapping("/test1_1")
    public String test1_1(Model model, HttpServletResponse response) {
        return "app/test1_1";
    }
    @RequestMapping("/test1_2")
    public String test1_2(Model model, HttpServletResponse response) {
        return "app/test1_2";
    }
    @RequestMapping("/test2")
    public String test2(Model model, HttpServletResponse response) {
        return "app/test2";
    }
    @RequestMapping("/test2_1")
    public String test2_1(Model model, HttpServletResponse response) {
        return "app/test2_1";
    }
    @RequestMapping("/test2_2")
    public String test2_2(Model model, HttpServletResponse response) {
        return "app/test2_2";
    }
    @RequestMapping("/test2_3")
    public String test2_3(Model model, HttpServletResponse response) {
        return "app/test2_3";
    }
    @RequestMapping("/test3")
    public String test3(Model model, HttpServletResponse response) {
        return "app/test3";
    }
    @RequestMapping("/test3_1")
    public String test3_1(Model model, HttpServletResponse response) {
        return "app/test3_1";
    }
}
