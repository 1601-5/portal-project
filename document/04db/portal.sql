drop table if exists app;

drop table if exists dept;

drop table if exists login;

drop table if exists module;

drop table if exists person;

drop table if exists personrole;

drop table if exists role;

drop table if exists roleright;

/*==============================================================*/
/* Table: app                                                   */
/*==============================================================*/
create table app
(
   app_Id               int(10) not null AUTO_INCREMENT,
   app_name             varchar(50),
   app_num              varchar(50),
   app_url              varchar(50),
   primary key (app_Id)
)DEFAULT CHARSET=utf8 ;

/*==============================================================*/
/* Table: dept                                                  */
/*==============================================================*/
create table dept
(
   dept_Id              int(10) not null AUTO_INCREMENT,
   dept_name            varchar(50),
   dept_father          int(10),
   dept_num             varchar(50),
   dept_site            varchar(50),
   dept_create          varchar(50),
   primary key (dept_Id)
)DEFAULT CHARSET=utf8 ;

/*==============================================================*/
/* Table: log                                                   */
/*==============================================================*/
create table login
(
    person_Id            int(10) not null AUTO_INCREMENT,
	user_Id              varchar(50) UNIQUE,
    password             varchar(50),
	is_admin             varchar(1),
	primary key (person_Id)
)DEFAULT CHARSET=utf8 ;

/*==============================================================*/
/* Table: module                                                */
/*==============================================================*/
create table module
(
   module_Id            int(10) not null AUTO_INCREMENT,
   app_Id               int(10) not null,
   module_name          varchar(50),
   module_num           varchar(50),
   module_url           varchar(50),
   module_create        varchar(50),
   primary key (module_Id)
)DEFAULT CHARSET=utf8 ;

/*==============================================================*/
/* Table: person                                                */
/*==============================================================*/
create table person
(
   person_Id            int(10) not null AUTO_INCREMENT,
   dept_Id              int(10),
   person_name          varchar(50),
   person_tel           varchar(50),
   person_sex           varchar(1),
   person_entertime     varchar(50),
   person_pay           varchar(50),
   primary key (person_Id)
)DEFAULT CHARSET=utf8 ;

/*==============================================================*/
/* Table: personrole                                            */
/*==============================================================*/
create table personrole
(
   pr_Id                int(10) not null AUTO_INCREMENT,
   person_Id            int(10),
   role_Id              int(10),
   primary key (pr_Id)
)DEFAULT CHARSET=utf8 ;

/*==============================================================*/
/* Table: role                                                  */
/*==============================================================*/
create table role
(
   role_Id              int(10) not null AUTO_INCREMENT,
   role_name            varchar(50),
   role_num             varchar(50),
   role_create          varchar(50),
   primary key (role_Id)
)DEFAULT CHARSET=utf8 ;

/*==============================================================*/
/* Table: roleright                                             */
/*==============================================================*/
create table roleright
(
   rm_Id                int(10) not null AUTO_INCREMENT,
   role_Id              int(10),
   module_Id            int(10),
   primary key (rm_Id)
)DEFAULT CHARSET=utf8 ;